package com.sys.web.controller.product;

import com.sys.common.annotation.Log;
import com.sys.common.core.controller.BaseController;
import com.sys.common.core.domain.AjaxResult;
import com.sys.common.core.page.TableDataInfo;
import com.sys.common.enums.BusinessType;
import com.sys.common.enums.ImportStatus;
import com.sys.common.utils.poi.ExcelUtil;
import com.sys.product.domain.ProductType;
import com.sys.product.domain.dto.ProductTypeImportDto;
import com.sys.product.domain.vo.DictVo;
import com.sys.product.service.IProductTypeService;
import com.sys.system.domain.SysImportProcess;
import com.sys.system.service.ISysImportProcessService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;


@Slf4j
@RestController
@RequestMapping("/product/productType")
@RequiredArgsConstructor
public class ProductTypeController extends BaseController {

    private final IProductTypeService productTypeService;
    private final ISysImportProcessService sysImportProcessService;

    @GetMapping("/listDict")
    public AjaxResult listDict() {
        List<DictVo<Integer>> list = productTypeService.selectListDict();
        return success(list);
    }

    /**
     * 查询产品类型列表
     */
    @PreAuthorize("@ss.hasPermi('product:productType:list')")
    @GetMapping("/list")
    public TableDataInfo list(ProductType productType) {
        startPage();
        List<ProductType> list = productTypeService.selectList(productType);
        return getDataTable(list);
    }

    /**
     * 获取产品类型详细信息
     */
    @PreAuthorize("@ss.hasPermi('product:productType:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return success(productTypeService.selectById(id));
    }

    /**
     * 新增产品类型
     */
    @PreAuthorize("@ss.hasPermi('product:productType:add')")
    @Log(title = "产品类型", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ProductType productType) {
        return toAjax(productTypeService.insert(productType));
    }

    /**
     * 修改产品类型
     */
    @PreAuthorize("@ss.hasPermi('product:productType:edit')")
    @Log(title = "产品类型", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ProductType productType) {
        return toAjax(productTypeService.update(productType));
    }

    /**
     * 删除产品类型
     */
    @PreAuthorize("@ss.hasPermi('product:productType:remove')")
    @Log(title = "产品类型", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(productTypeService.deleteByIds(ids));
    }

    /**
     * 下载导入模板
     *
     * @param response
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) {
        ExcelUtil<ProductTypeImportDto> util = new ExcelUtil<>(ProductTypeImportDto.class);
        util.importTemplateExcel(response, "产品设置");
    }

    @Log(title = "产品类型", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('product:productType:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file) throws Exception {
        SysImportProcess process = SysImportProcess.builder().totalRow(123).completeRow(45).status(ImportStatus.RUNNING).build();
        Long importId = sysImportProcessService.insert(process);
        return success(importId);
    }


}
