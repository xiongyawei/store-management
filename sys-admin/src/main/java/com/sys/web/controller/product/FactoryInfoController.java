package com.sys.web.controller.product;

import com.sys.common.annotation.Log;
import com.sys.common.core.controller.BaseController;
import com.sys.common.core.domain.AjaxResult;
import com.sys.common.core.page.TableDataInfo;
import com.sys.common.enums.BusinessType;
import com.sys.common.utils.poi.ExcelUtil;
import com.sys.product.domain.FactoryInfo;
import com.sys.product.domain.vo.DictVo;
import com.sys.product.service.IFactoryInfoService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@Slf4j
@RestController
@RequestMapping("/product/factoryInfo")
@RequiredArgsConstructor
public class FactoryInfoController extends BaseController {

    private final IFactoryInfoService factoryInfoService;

    @GetMapping("/listDict")
    public AjaxResult listDict() {
        List<DictVo<Integer>> list = factoryInfoService.selectListDict();
        return success(list);
    }

    /**
     * 查询厂家信息列表
     */
    @PreAuthorize("@ss.hasPermi('product:factoryInfo:list')")
    @GetMapping("/list")
    public TableDataInfo list(FactoryInfo factoryInfo) {
        startPage();
        List<FactoryInfo> list = factoryInfoService.selectList(factoryInfo);
        return getDataTable(list);
    }

    /**
     * 导出厂家信息列表
     */
//    @PreAuthorize("@ss.hasPermi('product:factoryInfo:export')")
//    @Log(title = "厂家信息", businessType = BusinessType.EXPORT)
//    @PostMapping("/export")
//    public void export(HttpServletResponse response, FactoryInfo factoryInfo) {
//        List<FactoryInfo> list = factoryInfoService.selectFactoryInfoList(factoryInfo);
//        ExcelUtil<FactoryInfo> util = new ExcelUtil<FactoryInfo>(FactoryInfo.class);
//        util.exportExcel(response, list, "厂家信息数据");
//    }

    /**
     * 获取厂家信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('product:factoryInfo:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return success(factoryInfoService.selectById(id));
    }

    /**
     * 新增厂家信息
     */
    @PreAuthorize("@ss.hasPermi('product:factoryInfo:add')")
    @Log(title = "厂家信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FactoryInfo factoryInfo) {
        return toAjax(factoryInfoService.insert(factoryInfo));
    }

    /**
     * 修改厂家信息
     */
    @PreAuthorize("@ss.hasPermi('product:factoryInfo:edit')")
    @Log(title = "厂家信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FactoryInfo factoryInfo) {
        return toAjax(factoryInfoService.updateById(factoryInfo));
    }

    /**
     * 删除厂家信息
     */
    @PreAuthorize("@ss.hasPermi('product:factoryInfo:remove')")
    @Log(title = "厂家信息", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(factoryInfoService.deleteByIds(ids));
    }


}
