package com.sys.web.controller.system;

import com.sys.common.core.controller.BaseController;
import com.sys.common.core.domain.AjaxResult;
import com.sys.system.service.ISysImportProcessService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/import")
@RequiredArgsConstructor
public class ImportProcessController extends BaseController {

    private final ISysImportProcessService sysImportProcessService;

    @GetMapping(value = "/{importId}")
    public AjaxResult getInfo(@PathVariable Long importId) {
        return success(sysImportProcessService.selectById(importId));
    }

}
