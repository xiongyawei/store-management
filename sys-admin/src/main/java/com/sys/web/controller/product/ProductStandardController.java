package com.sys.web.controller.product;

import java.util.List;

import com.sys.product.domain.ProductStandard;
import com.sys.product.domain.vo.DictVo;
import com.sys.product.service.IProductStandardService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.sys.common.annotation.Log;
import com.sys.common.core.controller.BaseController;
import com.sys.common.core.domain.AjaxResult;
import com.sys.common.enums.BusinessType;
import com.sys.common.core.page.TableDataInfo;

/**
 * 产品规格Controller
 *
 * @author ruoyi
 * @date 2023-11-01
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/product/standard")
public class ProductStandardController extends BaseController
{
    private final IProductStandardService productStandardService;

    @GetMapping("/listDict")
    public AjaxResult listDict(Long productTypeId) {
        List<DictVo<Integer>> list = productStandardService.selectListDict(productTypeId);
        return success(list);
    }

    /**
     * 查询产品规格列表
     */
    @PreAuthorize("@ss.hasPermi('product:standard:list')")
    @GetMapping("/list")
    public TableDataInfo list(ProductStandard productStandard)
    {
        startPage();
        List<ProductStandard> list = productStandardService.selectList(productStandard);
        return getDataTable(list);
    }

    /**
     * 获取产品规格详细信息
     */
    @PreAuthorize("@ss.hasPermi('product:standard:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(productStandardService.selectById(id));
    }

    /**
     * 新增产品规格
     */
    @PreAuthorize("@ss.hasPermi('product:standard:add')")
    @Log(title = "产品规格", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ProductStandard productStandard)
    {
        return toAjax(productStandardService.insert(productStandard));
    }

    /**
     * 修改产品规格
     */
    @PreAuthorize("@ss.hasPermi('product:standard:edit')")
    @Log(title = "产品规格", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ProductStandard productStandard)
    {
        return toAjax(productStandardService.update(productStandard));
    }

    /**
     * 删除产品规格
     */
    @PreAuthorize("@ss.hasPermi('product:standard:remove')")
    @Log(title = "产品规格", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(productStandardService.deleteByIds(ids));
    }
}
