package com.sys.web.controller.product;


import com.sys.product.domain.ProductScheme;
import com.sys.product.domain.vo.DictVo;
import com.sys.product.service.IProductSchemeService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.sys.common.annotation.Log;
import com.sys.common.core.controller.BaseController;
import com.sys.common.core.domain.AjaxResult;
import com.sys.common.enums.BusinessType;
import com.sys.common.core.page.TableDataInfo;

import java.util.List;

/**
 * 产品方案Controller
 *
 * @author ruoyi
 * @date 2023-11-01
 */
@RestController
@RequestMapping("/product/scheme")
@RequiredArgsConstructor
public class ProductSchemeController extends BaseController {
    private final IProductSchemeService productSchemeService;

    @GetMapping("/listDict")
    public AjaxResult listDict(Long productTypeId) {
        List<DictVo<Integer>> list = productSchemeService.selectListDict(productTypeId);
        return success(list);
    }

    /**
     * 查询产品方案列表
     */
    @PreAuthorize("@ss.hasPermi('product:scheme:list')")
    @GetMapping("/list")
    public TableDataInfo list(ProductScheme productScheme) {
        startPage();
        List<ProductScheme> list = productSchemeService.selectList(productScheme);
        return getDataTable(list);
    }

    /**
     * 获取产品方案详细信息
     */
    @PreAuthorize("@ss.hasPermi('product:scheme:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return success(productSchemeService.selectById(id));
    }

    /**
     * 新增产品方案
     */
    @PreAuthorize("@ss.hasPermi('product:scheme:add')")
    @Log(title = "产品方案", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ProductScheme productScheme) {
        return toAjax(productSchemeService.insert(productScheme));
    }

    /**
     * 修改产品方案
     */
    @PreAuthorize("@ss.hasPermi('product:scheme:edit')")
    @Log(title = "产品方案", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ProductScheme productScheme) {
        return toAjax(productSchemeService.update(productScheme));
    }

    /**
     * 删除产品方案
     */
    @PreAuthorize("@ss.hasPermi('product:scheme:remove')")
    @Log(title = "产品方案", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(productSchemeService.deleteByIds(ids));
    }
}
