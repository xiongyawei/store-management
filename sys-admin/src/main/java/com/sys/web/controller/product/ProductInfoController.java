package com.sys.web.controller.product;

import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson2.JSON;
import com.sys.product.domain.ProductInfo;
import com.sys.product.domain.dto.ProductInfoDto;
import com.sys.product.domain.vo.ProductInfoVo;
import com.sys.product.service.IProductInfoService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.sys.common.annotation.Log;
import com.sys.common.core.controller.BaseController;
import com.sys.common.core.domain.AjaxResult;
import com.sys.common.enums.BusinessType;
import com.sys.common.core.page.TableDataInfo;

/**
 * 产品信息Controller
 *
 * @author ruoyi
 * @date 2023-11-02
 */
@Slf4j
@RestController
@RequestMapping("/product/productInfo")
@RequiredArgsConstructor
public class ProductInfoController extends BaseController {
    private final IProductInfoService productInfoService;

    /**
     * 查询产品信息列表
     */
    @PreAuthorize("@ss.hasPermi('product:productInfo:list')")
    @GetMapping("/list")
    public TableDataInfo list(ProductInfo productInfo) {
        startPage();
        List<ProductInfoVo> list = productInfoService.selectList(productInfo);
        return getDataTable(list);
    }

//    /**
//     * 导出产品信息列表
//     */
//    @PreAuthorize("@ss.hasPermi('product:productInfo:export')")
//    @Log(title = "产品信息", businessType = BusinessType.EXPORT)
//    @PostMapping("/export")
//    public void export(HttpServletResponse response, ProductInfo productInfo)
//    {
//        List<ProductInfo> list = productInfoService.selectProductInfoList(productInfo);
//        ExcelUtil<ProductInfo> util = new ExcelUtil<ProductInfo>(ProductInfo.class);
//        util.exportExcel(response, list, "产品信息数据");
//    }

    /**
     * 获取产品信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('product:productInfo:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return success(productInfoService.selectById(id));
    }

    /**
     * 新增产品信息
     */
    @PreAuthorize("@ss.hasPermi('product:productInfo:add')")
    @Log(title = "产品信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ProductInfo productInfo) {
        return toAjax(productInfoService.insert(productInfo));
    }

    /**
     * 修改产品信息
     */
    @PreAuthorize("@ss.hasPermi('product:productInfo:edit')")
    @Log(title = "产品信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ProductInfo productInfo) {
        return toAjax(productInfoService.update(productInfo));
    }

    /**
     * 删除产品信息
     */
    @PreAuthorize("@ss.hasPermi('product:productInfo:remove')")
    @Log(title = "产品信息", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(productInfoService.deleteByIds(ids));
    }


    @PostMapping("/loadProductData")
    public TableDataInfo loadProductData(@RequestBody ProductInfoDto productInfoDto) {

        List<ProductInfoVo> list = productInfoService.loadProductData(productInfoDto.getProductTypeId(), productInfoDto.getSchemeList(), productInfoDto.getStandardList());
        return getDataTable(list);
    }

}
