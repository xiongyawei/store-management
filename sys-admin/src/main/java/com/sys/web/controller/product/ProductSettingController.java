package com.sys.web.controller.product;

import com.sys.common.core.controller.BaseController;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 产品设置
 */
@Slf4j
@RestController
@RequestMapping("/product/productSetting")
@RequiredArgsConstructor
public class ProductSettingController extends BaseController {


}
