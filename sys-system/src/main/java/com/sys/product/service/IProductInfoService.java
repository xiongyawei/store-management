package com.sys.product.service;

import com.sys.product.domain.ProductInfo;
import com.sys.product.domain.vo.ProductInfoVo;

import java.util.List;

public interface IProductInfoService {
    List<ProductInfoVo> selectList(ProductInfo productInfo);

    ProductInfo selectById(Long id);

    int insert(ProductInfo productInfo);

    int update(ProductInfo productInfo);

    int deleteByIds(Long[] ids);

    List<ProductInfoVo> loadProductData(Long productTypeId, List<Long> schemeList, List<Long> standardList);
}
