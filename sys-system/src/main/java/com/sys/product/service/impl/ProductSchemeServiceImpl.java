package com.sys.product.service.impl;

import cn.hutool.core.util.ObjUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.sys.common.utils.StringUtils;
import com.sys.product.domain.ProductScheme;
import com.sys.product.domain.vo.DictVo;
import com.sys.product.mapper.ProductSchemeMapper;
import com.sys.product.service.IProductSchemeService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class ProductSchemeServiceImpl implements IProductSchemeService {

    private final ProductSchemeMapper productSchemeMapper;

    @Override
    public List<ProductScheme> selectList(ProductScheme productScheme) {
        LambdaQueryWrapper<ProductScheme> queryWrapper = Wrappers.<ProductScheme>lambdaQuery()
                .eq(ObjUtil.isNotNull(productScheme.getProductTypeId()), ProductScheme::getProductTypeId, productScheme.getProductTypeId())
                .like(StringUtils.isNotBlank(productScheme.getSchemeName()), ProductScheme::getSchemeName, productScheme.getSchemeName())
                .orderByAsc(ProductScheme::getSort);
        return productSchemeMapper.selectList(queryWrapper);
    }

    @Override
    public ProductScheme selectById(Long id) {
        return productSchemeMapper.selectById(id);
    }

    @Override
    public int insert(ProductScheme productScheme) {
        return productSchemeMapper.insert(productScheme);
    }

    @Override
    public int update(ProductScheme productScheme) {
        return productSchemeMapper.updateById(productScheme);
    }

    @Override
    public int deleteByIds(Long[] ids) {
        return productSchemeMapper.deleteBatchIds(Arrays.stream(ids).collect(Collectors.toList()));
    }

    @Override
    public List<DictVo<Integer>> selectListDict(Long productTypeId) {
        return productSchemeMapper.selectList(
                        Wrappers.<ProductScheme>lambdaQuery()
                                .eq(ProductScheme::getProductTypeId, productTypeId)
                                .orderByAsc(ProductScheme::getSort)
                ).stream()
                .map(
                        item -> DictVo.<Integer>builder()
                                .label(item.getSchemeName())
                                .value(item.getId().intValue())
                                .build()).collect(Collectors.toList());
    }
}
