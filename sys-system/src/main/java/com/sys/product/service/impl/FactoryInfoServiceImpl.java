package com.sys.product.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.sys.common.utils.StringUtils;
import com.sys.product.domain.FactoryInfo;
import com.sys.product.domain.vo.DictVo;
import com.sys.product.mapper.FactoryInfoMapper;
import com.sys.product.service.IFactoryInfoService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.xmlbeans.XmlID;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class FactoryInfoServiceImpl implements IFactoryInfoService {

    private final FactoryInfoMapper factoryInfoMapper;

    @Override
    public List<FactoryInfo> selectList(FactoryInfo factoryInfo) {
        LambdaQueryWrapper<FactoryInfo> queryWrapper = Wrappers.<FactoryInfo>lambdaQuery()
                .like(StringUtils.isNotBlank(factoryInfo.getName()), FactoryInfo::getName, factoryInfo.getName())
                .orderByAsc(FactoryInfo::getName);
        return factoryInfoMapper.selectList(queryWrapper);
    }

    @Override
    public FactoryInfo selectById(Long id) {
        return factoryInfoMapper.selectById(id);
    }

    @Override
    public int insert(FactoryInfo factoryInfo) {
        return factoryInfoMapper.insert(factoryInfo);
    }

    @Override
    public int updateById(FactoryInfo factoryInfo) {
        return factoryInfoMapper.updateById(factoryInfo);
    }

    @Override
    public int deleteByIds(Long[] ids) {
        return factoryInfoMapper.deleteBatchIds(Arrays.stream(ids).collect(Collectors.toList()));
    }

    @Override
    public List<DictVo<Integer>> selectListDict() {
        return factoryInfoMapper.selectList(null)
                .stream()
                .map(
                        item -> DictVo.<Integer>builder()
                                .label(item.getName())
                                .value(item.getId().intValue())
                                .build()).collect(Collectors.toList());
    }
}
