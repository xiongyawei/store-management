package com.sys.product.service;

import com.sys.product.domain.ProductScheme;
import com.sys.product.domain.vo.DictVo;

import java.util.List;

public interface IProductSchemeService {
    List<ProductScheme> selectList(ProductScheme productScheme);

    ProductScheme selectById(Long id);

    int insert(ProductScheme productScheme);

    int update(ProductScheme productScheme);

    int deleteByIds(Long[] ids);

    List<DictVo<Integer>> selectListDict(Long productTypeId);
}
