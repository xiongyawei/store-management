package com.sys.product.service.impl;

import cn.hutool.core.util.ObjUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.sys.common.utils.StringUtils;
import com.sys.product.domain.ProductScheme;
import com.sys.product.domain.ProductStandard;
import com.sys.product.domain.vo.DictVo;
import com.sys.product.mapper.ProductStandardMapper;
import com.sys.product.service.IProductStandardService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class ProductStandardServiceImpl implements IProductStandardService {

    private final ProductStandardMapper productStandardMapper;

    @Override
    public List<ProductStandard> selectList(ProductStandard productStandard) {
        LambdaQueryWrapper<ProductStandard> queryWrapper = Wrappers.<ProductStandard>lambdaQuery()
                .eq(ObjUtil.isNotNull(productStandard.getProductTypeId()), ProductStandard::getProductTypeId, productStandard.getProductTypeId())
                .like(StringUtils.isNotBlank(productStandard.getStandardName()), ProductStandard::getStandardName, productStandard.getStandardName())
                .orderByAsc(ProductStandard::getSort);
        return productStandardMapper.selectList(queryWrapper);
    }

    @Override
    public ProductStandard selectById(Long id) {
        return productStandardMapper.selectById(id);
    }

    @Override
    public int insert(ProductStandard productStandard) {
        return productStandardMapper.insert(productStandard);
    }

    @Override
    public int update(ProductStandard productStandard) {
        return productStandardMapper.updateById(productStandard);
    }

    @Override
    public int deleteByIds(Long[] ids) {
        return productStandardMapper.deleteBatchIds(Arrays.stream(ids).collect(Collectors.toList()));
    }

    @Override
    public List<DictVo<Integer>> selectListDict(Long productTypeId) {
        return productStandardMapper.selectList(
                        Wrappers.<ProductStandard>lambdaQuery()
                                .eq(ProductStandard::getProductTypeId, productTypeId)
                                .orderByAsc(ProductStandard::getSort)
                ).stream()
                .map(
                        item -> DictVo.<Integer>builder()
                                .label(item.getStandardName())
                                .value(item.getId().intValue())
                                .build()).collect(Collectors.toList());
    }
}
