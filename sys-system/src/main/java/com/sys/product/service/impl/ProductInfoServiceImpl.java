package com.sys.product.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjUtil;
import com.github.yulichang.toolkit.JoinWrappers;
import com.github.yulichang.wrapper.MPJLambdaWrapper;
import com.sys.product.domain.*;
import com.sys.product.domain.vo.ProductInfoVo;
import com.sys.product.mapper.ProductInfoMapper;
import com.sys.product.service.IProductInfoService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class ProductInfoServiceImpl implements IProductInfoService {

    private final ProductInfoMapper productInfoMapper;

    @Override
    public List<ProductInfoVo> selectList(ProductInfo productInfo) {
        MPJLambdaWrapper<ProductInfo> wrapper = JoinWrappers.lambda(ProductInfo.class)
                .selectAll(ProductInfo.class)
                .selectAs(FactoryInfo::getName, ProductInfoVo::getFactoryName)
                .selectAs(ProductType::getProductTypeName, ProductInfoVo::getProductTypeName)
                .selectAs(ProductScheme::getSchemeName, ProductInfoVo::getProductSchemeName)
                .selectAs(ProductStandard::getStandardName, ProductInfoVo::getProductStandardName)
                .leftJoin(ProductType.class, ProductType::getId, ProductInfo::getProductTypeId)
                .leftJoin(ProductScheme.class, ProductScheme::getId, ProductInfo::getProductSchemeId)
                .leftJoin(ProductStandard.class, ProductStandard::getId, ProductInfo::getProductStandardId)
                .leftJoin(FactoryInfo.class, FactoryInfo::getId, ProductInfo::getFactoryId)
                .eq(ObjUtil.isNotNull(productInfo.getFactoryId()), ProductInfo::getFactoryId, productInfo.getFactoryId())
                .eq(ObjUtil.isNotNull(productInfo.getProductTypeId()), ProductInfo::getProductTypeId, productInfo.getProductTypeId())
                .eq(ObjUtil.isNotNull(productInfo.getProductSchemeId()), ProductInfo::getProductSchemeId, productInfo.getProductSchemeId())
                .eq(ObjUtil.isNotNull(productInfo.getProductStandardId()), ProductInfo::getProductStandardId, productInfo.getProductStandardId())
                .orderByDesc(ProductInfo::getCreateTime);
        return productInfoMapper.selectJoinList(ProductInfoVo.class, wrapper);
    }

    @Override
    public ProductInfo selectById(Long id) {
        return productInfoMapper.selectById(id);
    }

    @Override
    public int insert(ProductInfo productInfo) {
        return productInfoMapper.insert(productInfo);
    }

    @Override
    public int update(ProductInfo productInfo) {
        return productInfoMapper.updateById(productInfo);
    }

    @Override
    public int deleteByIds(Long[] ids) {
        return productInfoMapper.deleteBatchIds(Arrays.stream(ids).collect(Collectors.toList()));
    }

    @Override
    public List<ProductInfoVo> loadProductData(Long productTypeId, List<Long> schemeList, List<Long> standardList) {
        MPJLambdaWrapper<ProductInfo> wrapper = JoinWrappers.lambda(ProductInfo.class)
                .selectAll(ProductInfo.class)
                .selectAs(FactoryInfo::getName, ProductInfoVo::getFactoryName)
                .selectAs(ProductType::getProductTypeName, ProductInfoVo::getProductTypeName)
                .selectAs(ProductScheme::getSchemeName, ProductInfoVo::getProductSchemeName)
                .selectAs(ProductStandard::getStandardName, ProductInfoVo::getProductStandardName)
                .leftJoin(ProductType.class, ProductType::getId, ProductInfo::getProductTypeId)
                .leftJoin(ProductScheme.class, ProductScheme::getId, ProductInfo::getProductSchemeId)
                .leftJoin(ProductStandard.class, ProductStandard::getId, ProductInfo::getProductStandardId)
                .leftJoin(FactoryInfo.class, FactoryInfo::getId, ProductInfo::getFactoryId)
                .eq(ProductInfo::getProductTypeId, productTypeId)
                .in(!CollUtil.isEmpty(schemeList), ProductInfo::getProductSchemeId, schemeList)
                .in(!CollUtil.isEmpty(standardList), ProductInfo::getProductStandardId, standardList)
                .orderByDesc(ProductScheme::getSort)
                .orderByDesc(ProductStandard::getSort);
        return productInfoMapper.selectJoinList(ProductInfoVo.class, wrapper);
    }
}
