package com.sys.product.service;

import com.sys.product.domain.FactoryInfo;
import com.sys.product.domain.vo.DictVo;

import java.util.List;

public interface IFactoryInfoService{
    List<FactoryInfo> selectList(FactoryInfo factoryInfo);

    FactoryInfo selectById(Long id);

    int insert(FactoryInfo factoryInfo);

    int updateById(FactoryInfo factoryInfo);

    int deleteByIds(Long[] ids);

    List<DictVo<Integer>> selectListDict();
}
