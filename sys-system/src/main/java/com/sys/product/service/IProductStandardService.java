package com.sys.product.service;

import com.sys.product.domain.ProductStandard;
import com.sys.product.domain.vo.DictVo;

import java.util.List;

public interface IProductStandardService {
    List<ProductStandard> selectList(ProductStandard productStandard);

    ProductStandard selectById(Long id);

    int insert(ProductStandard productStandard);

    int update(ProductStandard productStandard);

    int deleteByIds(Long[] ids);

    List<DictVo<Integer>> selectListDict(Long productTypeId);
}
