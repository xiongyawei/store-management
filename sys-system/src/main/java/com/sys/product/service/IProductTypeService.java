package com.sys.product.service;

import com.sys.product.domain.ProductType;
import com.sys.product.domain.vo.DictVo;

import java.util.List;

public interface IProductTypeService {
    List<ProductType> selectList(ProductType productType);

    ProductType selectById(Long id);

    int insert(ProductType productType);

    int update(ProductType productType);

    int deleteByIds(Long[] ids);

    List<DictVo<Integer>> selectListDict();
}
