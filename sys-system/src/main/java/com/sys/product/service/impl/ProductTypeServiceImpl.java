package com.sys.product.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.sys.common.utils.StringUtils;
import com.sys.product.domain.ProductType;
import com.sys.product.domain.vo.DictVo;
import com.sys.product.mapper.ProductTypeMapper;
import com.sys.product.service.IProductTypeService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class ProductTypeServiceImpl implements IProductTypeService {

    private final ProductTypeMapper productTypeMapper;

    @Override
    public List<ProductType> selectList(ProductType productType) {
        LambdaQueryWrapper<ProductType> queryWrapper = Wrappers.<ProductType>lambdaQuery()
                .like(StringUtils.isNotBlank(productType.getProductTypeName()), ProductType::getProductTypeName, productType.getProductTypeName())
                .orderByAsc(ProductType::getSort);

        return productTypeMapper.selectList(queryWrapper);
    }

    @Override
    public ProductType selectById(Long id) {
        return productTypeMapper.selectById(id);
    }

    @Override
    public int insert(ProductType productType) {
        return productTypeMapper.insert(productType);
    }

    @Override
    public int update(ProductType productType) {
        return productTypeMapper.updateById(productType);
    }

    @Override
    public int deleteByIds(Long[] ids) {
        return productTypeMapper.deleteBatchIds(Arrays.stream(ids).collect(Collectors.toList()));
    }

    @Override
    public List<DictVo<Integer>> selectListDict() {
        return productTypeMapper.selectList(null)
                .stream()
                .map(
                        item -> DictVo.<Integer>builder()
                                .label(item.getProductTypeName())
                                .value(item.getId().intValue())
                                .build()).collect(Collectors.toList());
    }
}
