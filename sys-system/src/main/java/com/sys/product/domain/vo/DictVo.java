package com.sys.product.domain.vo;

import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class DictVo<T> implements Serializable {
    private static final long serialVersionUID = -6542862739713018611L;

    private String label;

    private T value;

    private String remark;

}
