package com.sys.product.domain.dto;

import lombok.*;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProductInfoDto implements Serializable {
    private static final long serialVersionUID = -2879726959959504237L;

    private Long productTypeId;
    private List<Long> schemeList;
    private List<Long> standardList;

}
