package com.sys.product.domain;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.sys.common.core.domain.BaseEntity;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@TableName("product_scheme")
public class ProductScheme extends BaseEntity {

    private static final long serialVersionUID = 2746086752671102732L;
    @TableId
    private Long id;

    /**
     * 产品类型ID
     */
    private Long productTypeId;

    /**
     * 方案名称
     */
    private String schemeName;

    /**
     * 排序
     */
    private Integer sort;

    @TableLogic
    private boolean delFlag;

}
