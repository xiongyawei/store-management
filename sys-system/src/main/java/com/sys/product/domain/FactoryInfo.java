package com.sys.product.domain;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.sys.common.core.domain.BaseEntity;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@TableName("factory_info")
public class FactoryInfo extends BaseEntity {

    private static final long serialVersionUID = 3491844207573365814L;
    @TableId
    private Long id;

    /**
     * 厂家名称
     */
    private String name;

    /**
     * 联系人
     */
    private String contactPerson;

    /**
     * 联系人电话
     */
    private String contactPhone;

    /**
     * 联系人微信
     */
    private String contactWechat;

    /**
     * 地址
     */
    private String address;

    @TableLogic
    private boolean delFlag;

}
