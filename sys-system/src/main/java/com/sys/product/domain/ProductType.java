package com.sys.product.domain;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.sys.common.core.domain.BaseEntity;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@TableName("product_type")
public class ProductType extends BaseEntity {
    private static final long serialVersionUID = -5328023310695052564L;
    @TableId
    private Long id;

    /**
     * 产品类型名称
     */
    private String productTypeName;

    /**
     * 排序
     */
    private Integer sort;

    @TableLogic
    private boolean delFlag;
}
