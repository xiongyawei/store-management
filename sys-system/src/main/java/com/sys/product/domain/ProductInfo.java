package com.sys.product.domain;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.sys.common.core.domain.BaseEntity;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@TableName("product_info")
public class ProductInfo extends BaseEntity {

    private static final long serialVersionUID = -4280079426786969709L;
    @TableId
    private Long id;

    /**
     * 所属厂家ID
     */
    private Long factoryId;

    /**
     * 产品图片路径
     */
    private String productImg;

    /**
     * 产品类型ID
     */
    private Long productTypeId;

    /**
     * 产品规格ID
     */
    private Long productStandardId;

    /**
     * 产品方案ID
     */
    private Long productSchemeId;

    /**
     * 产品尺寸
     */
    private String size;

    /**
     * 价格
     */
    private BigDecimal price;

    /**
     * 产品重量
     */
    private Double weight;

    /**
     * 重量单位
     */
    private String weightUnit;

    /**
     * 装箱数量
     */
    private Integer boxNum;

    /**
     * 外箱长（米）
     */
    private Double boxLong;

    /**
     * 外箱宽（米）
     */
    private Double boxWide;


    /**
     * 外箱高度（米）
     */
    private Double boxHeight;

    /**
     * 外箱体积（立方米）
     */
    private Double boxVolume;

    /**
     * 装箱重量（千克）
     */
    private Double boxWeight;

    @TableLogic
    private boolean delFlag;

}
