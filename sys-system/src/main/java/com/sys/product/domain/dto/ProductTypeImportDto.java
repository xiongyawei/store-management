package com.sys.product.domain.dto;

import com.sys.common.annotation.Excel;
import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ProductTypeImportDto implements Serializable {
    private static final long serialVersionUID = 5577620657638287704L;

    @Excel(name = "产品类型")
    private String productTypeName;

    @Excel(name = "产品方案")
    private String productSchemeName;

    @Excel(name = "产品规格")
    private String productStandardName;

}
