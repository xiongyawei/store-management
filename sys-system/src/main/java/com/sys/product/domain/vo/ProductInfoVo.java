package com.sys.product.domain.vo;

import com.sys.product.domain.ProductInfo;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProductInfoVo extends ProductInfo {
    private static final long serialVersionUID = -1501218238198936919L;

    /**
     * 工厂名称
     */
    private String factoryName;

    /**
     * 产品类型名称
     */
    private String productTypeName;

    /**
     * 产品方案名称
     */
    private String productSchemeName;

    /**
     * 产品规格名称
     */
    private String productStandardName;

}
