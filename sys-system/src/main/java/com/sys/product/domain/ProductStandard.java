package com.sys.product.domain;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.sys.common.core.domain.BaseEntity;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@TableName("product_standard")
public class ProductStandard extends BaseEntity {

    private static final long serialVersionUID = -7380864561752886251L;
    @TableId
    private Long id;

    /**
     * 产品类型ID
     */
    private Long productTypeId;

    /**
     * 产品规格
     */
    private String standardName;

    /**
     * 排序
     */
    private Integer sort;

    @TableLogic
    private boolean delFlag;

}
