package com.sys.product.mapper;

import com.github.yulichang.base.MPJBaseMapper;
import com.sys.product.domain.FactoryInfo;

public interface FactoryInfoMapper extends MPJBaseMapper<FactoryInfo> {
}
