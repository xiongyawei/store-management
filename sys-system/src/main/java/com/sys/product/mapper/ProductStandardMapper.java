package com.sys.product.mapper;

import com.github.yulichang.base.MPJBaseMapper;
import com.sys.product.domain.ProductStandard;

public interface ProductStandardMapper extends MPJBaseMapper<ProductStandard> {
}
