package com.sys.product.mapper;

import com.github.yulichang.base.MPJBaseMapper;
import com.sys.product.domain.ProductInfo;

public interface ProductInfoMapper extends MPJBaseMapper<ProductInfo> {
}
