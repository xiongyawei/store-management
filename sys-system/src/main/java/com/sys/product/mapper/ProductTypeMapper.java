package com.sys.product.mapper;

import com.github.yulichang.base.MPJBaseMapper;
import com.sys.product.domain.ProductType;

public interface ProductTypeMapper extends MPJBaseMapper<ProductType> {
}
