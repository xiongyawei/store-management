package com.sys.product.mapper;

import com.github.yulichang.base.MPJBaseMapper;
import com.sys.product.domain.ProductScheme;

public interface ProductSchemeMapper extends MPJBaseMapper<ProductScheme> {
}
