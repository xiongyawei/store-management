package com.sys.sample.domain;

import com.sys.common.core.domain.BaseEntity;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class Order extends BaseEntity {

    private Long id;

    private String orderNo;

    private String orderName;

    private Date orderTime;


}
