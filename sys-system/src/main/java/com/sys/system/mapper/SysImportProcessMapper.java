package com.sys.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sys.system.domain.SysImportProcess;

public interface SysImportProcessMapper extends BaseMapper<SysImportProcess> {
}
