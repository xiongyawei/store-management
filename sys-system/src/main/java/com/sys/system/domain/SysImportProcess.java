package com.sys.system.domain;

import com.baomidou.mybatisplus.annotation.TableId;
import com.sys.common.core.domain.BaseEntity;
import com.sys.common.enums.ImportStatus;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SysImportProcess extends BaseEntity {

    private static final long serialVersionUID = -6136859450565627396L;

    @TableId
    private Long id;

    /**
     * 导入总行数
     */
    private Integer totalRow;

    /**
     * 已完成行数
     */
    private Integer completeRow;

    /**
     * 导入状态：导入中、导入成功、导入失败
     */
    private ImportStatus status;

    /**
     * 导入原始文件地址
     */
    private String uploadFile;

    /**
     * 导入失败详情文件
     */
    private String failExceptionFile;


}
