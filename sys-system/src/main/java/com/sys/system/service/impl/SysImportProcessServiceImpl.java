package com.sys.system.service.impl;

import com.sys.system.domain.SysImportProcess;
import com.sys.system.mapper.SysImportProcessMapper;
import com.sys.system.service.ISysImportProcessService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class SysImportProcessServiceImpl implements ISysImportProcessService {

    private final SysImportProcessMapper sysImportProcessMapper;


    @Override
    public SysImportProcess selectById(Long importId) {
        return sysImportProcessMapper.selectById(importId);
    }

    @Override
    public Long insert(SysImportProcess process) {
        sysImportProcessMapper.insert(process);
        return process.getId();
    }
}
