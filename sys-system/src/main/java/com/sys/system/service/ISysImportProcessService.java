package com.sys.system.service;

import com.sys.system.domain.SysImportProcess;

public interface ISysImportProcessService {
    SysImportProcess selectById(Long importId);

    Long insert(SysImportProcess process);
}
