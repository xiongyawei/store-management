package com.sys.common.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import lombok.Getter;
import lombok.Setter;

/**
 * 导入状态
 */
@Getter
public enum ImportStatus {
    OK(1, "导入成功"), FAIL(-1, "导入失败"), RUNNING(0, "导入中");

    @EnumValue
    private final Integer code;

    private final String info;

    ImportStatus(Integer code, String info) {
        this.code = code;
        this.info = info;
    }
}
