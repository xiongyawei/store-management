package com.sys.common.config;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.sys.common.core.domain.model.LoginUser;
import com.sys.common.utils.SecurityUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.util.Date;

@Slf4j
@Component
public class SysMetaObjectHandler implements MetaObjectHandler {
    @Override
    public void insertFill(MetaObject metaObject) {
        LoginUser user = SecurityUtils.getLoginUser();

        log.info("start insert fill ....");
        this.strictInsertFill(metaObject, "createTime", Date.class, new Date());
        this.strictInsertFill(metaObject, "updateTime", Date.class, new Date());
        this.strictInsertFill(metaObject, "createBy", user::getUsername, String.class);
        this.strictInsertFill(metaObject, "updateBy", user::getUsername, String.class);
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        LoginUser user = SecurityUtils.getLoginUser();
        log.info("start update fill ....");
        this.strictUpdateFill(metaObject, "updateTime", Date.class, new Date());
        this.strictUpdateFill(metaObject, "updateBy", user::getUsername, String.class);
    }
}
