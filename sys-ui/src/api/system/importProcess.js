import request from '@/utils/request'

// 查询菜单列表
export function getImportProcess(importId) {
  return request({
    url: '/import/' + importId,
    method: 'get'
  })
}
