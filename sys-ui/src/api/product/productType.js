import request from '@/utils/request'

// 查询产品类型列表
export function listProductType(query) {
  return request({
    url: '/product/productType/list',
    method: 'get',
    params: query
  })
}

// 查询产品类型详细
export function getProductType(id) {
  return request({
    url: '/product/productType/' + id,
    method: 'get'
  })
}

// 新增产品类型
export function addProductType(data) {
  return request({
    url: '/product/productType',
    method: 'post',
    data: data
  })
}

// 修改产品类型
export function updateProductType(data) {
  return request({
    url: '/product/productType',
    method: 'put',
    data: data
  })
}

// 删除产品类型
export function delProductType(id) {
  return request({
    url: '/product/productType/' + id,
    method: 'delete'
  })
}
