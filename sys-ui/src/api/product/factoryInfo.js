import request from '@/utils/request'

// 查询厂家信息列表
export function listFactoryInfo(query) {
  return request({
    url: '/product/factoryInfo/list',
    method: 'get',
    params: query
  })
}

// 查询厂家信息详细
export function getFactoryInfo(id) {
  return request({
    url: '/product/factoryInfo/' + id,
    method: 'get'
  })
}

// 新增厂家信息
export function addFactoryInfo(data) {
  return request({
    url: '/product/factoryInfo',
    method: 'post',
    data: data
  })
}

// 修改厂家信息
export function updateFactoryInfo(data) {
  return request({
    url: '/product/factoryInfo',
    method: 'put',
    data: data
  })
}

// 删除厂家信息
export function delFactoryInfo(id) {
  return request({
    url: '/product/factoryInfo/' + id,
    method: 'delete'
  })
}
