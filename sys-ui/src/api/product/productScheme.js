import request from '@/utils/request'

// 查询产品方案列表
export function listScheme(query) {
  return request({
    url: '/product/scheme/list',
    method: 'get',
    params: query
  })
}

// 查询产品方案详细
export function getScheme(id) {
  return request({
    url: '/product/scheme/' + id,
    method: 'get'
  })
}

// 新增产品方案
export function addScheme(data) {
  return request({
    url: '/product/scheme',
    method: 'post',
    data: data
  })
}

// 修改产品方案
export function updateScheme(data) {
  return request({
    url: '/product/scheme',
    method: 'put',
    data: data
  })
}

// 删除产品方案
export function delScheme(id) {
  return request({
    url: '/product/scheme/' + id,
    method: 'delete'
  })
}
