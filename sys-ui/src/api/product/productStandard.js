import request from '@/utils/request'

// 查询产品规格列表
export function listStandard(query) {
  return request({
    url: '/product/standard/list',
    method: 'get',
    params: query
  })
}

// 查询产品规格详细
export function getStandard(id) {
  return request({
    url: '/product/standard/' + id,
    method: 'get'
  })
}

// 新增产品规格
export function addStandard(data) {
  return request({
    url: '/product/standard',
    method: 'post',
    data: data
  })
}

// 修改产品规格
export function updateStandard(data) {
  return request({
    url: '/product/standard',
    method: 'put',
    data: data
  })
}

// 删除产品规格
export function delStandard(id) {
  return request({
    url: '/product/standard/' + id,
    method: 'delete'
  })
}
