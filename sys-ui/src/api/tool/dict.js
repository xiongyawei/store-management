import request from '@/utils/request'

export function listFactoryDict() {
  return request({
    url: '/product/factoryInfo/listDict',
    method: 'get'
  })
}

export function listProductTypeDict() {
  return request({
    url: '/product/productType/listDict',
    method: 'get'
  })
}

export function listProductSchemeDict(productTypeId) {
  return request({
    url: '/product/scheme/listDict',
    method: 'get',
    params: {'productTypeId': productTypeId}
  })
}

export function listProductStandardDict(productTypeId) {
  return request({
    url: '/product/standard/listDict',
    method: 'get',
    params: {'productTypeId': productTypeId}
  })
}
